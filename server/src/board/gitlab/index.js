function check({ id, projectId, token }) {
    const { ProjectIssueBoards } = require('gitlab');
    return new ProjectIssueBoards({ token }).show(projectId, id);
}

function getLabels({ id, projectId, token }) {
    const { Labels } = require('gitlab');
    return new Labels({ token }).all(projectId, id);
}

function createLabel() {
    throw new Error('Not implemented')
}

function getLists({ id, projectId, token }) {
    const { ProjectIssueBoards } = require('gitlab');
    return new ProjectIssueBoards({ token }).show(projectId, id).then(p => p.lists);
}

function createList() {
    throw new Error('Not implemented')
}

function getIssues({ id, projectId, token }) {
    const { Issues } = require('gitlab');
    return new Issues({ token }).all(projectId, id);
}

function createIssue() {
    throw new Error('Not implemented')
}

module.exports = {
    check,
    getLabels,
    createLabel,
    getLists,
    createList,
    getIssues,
    createIssue,
}
