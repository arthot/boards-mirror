const TRELLO = 'trello'
const GITLAB = 'gitlab'

const TYPES = {
    [TRELLO]: TRELLO,
    [GITLAB]: GITLAB,
}

function resolve(type) {
    switch (type) {
        case TRELLO: return require('./trello');
        case GITLAB: return require('./gitlab');
        default: throw new Error('Unknow board type');
    }
}

function check(board) {
    return resolve(board.type).check(board);
}

function getLabels(board) {
    return resolve(board.type).getLabels(board);
}

function createLabel(board, label) {
    return resolve(board.type).createLabel(board, label);
}

function getLists(board) {
    return resolve(board.type).getLists(board);
}

function createList(board, list) {
    return resolve(board.type).createList(board, list);
}

function getIssues(board) {
    return resolve(board.type).getIssues(board);
}

function createIssue(board, issue) {
    return resolve(board.type).createIssue(board, issue);
}

function updateIssue(board, id, issue) {
    return resolve(board.type).updateIssue(board, id, issue);
}

module.exports = {
    TYPES,
    check,
    getLabels,
    createLabel,
    getLists,
    createList,
    getIssues,
    createIssue,
    updateIssue,
}
