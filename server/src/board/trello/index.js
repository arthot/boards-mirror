const Trello = require('trello')
const request = require('request-promise-native')
const config = require('../../utils/config')

function check({ id, token }) {
    const trello = new Trello(config.integrations.trello.key, token);
    return trello.getBoardMembers(id);
}

function getLabels({ id, token }) {
    const trello = new Trello(config.integrations.trello.key, token);
    return trello.getLabelsForBoard(id);
}

function createLabel({ id, token }, { name }) {
    const options = {
        method: 'POST',
        url: `https://api.trello.com/1/boards/${id}/labels`,
        qs: { name, color: 'null', key: config.integrations.trello.key, token }
    };

    return request(options);
}

function getLists({ id, token }) {
    const trello = new Trello(config.integrations.trello.key, token);
    return trello.getListsOnBoard(id);
}

function createList({ id, token }, { name, pos }) {
    const options = {
        method: 'POST',
        url: `https://api.trello.com/1/boards/${id}/lists`,
        qs: { name, pos, key: config.integrations.trello.key, token }
    };

    return request(options);
}

function getIssues({ id, token }) {
    const trello = new Trello(config.integrations.trello.key, token);
    return trello.getCardsOnBoardWithExtraParams(id);
}

function createIssue({ token }, { iid, idList, name, desc, due, idMembers, idLabels }) {
    const options = {
        method: 'POST',
        json: true,
        url: 'https://api.trello.com/1/cards',
        qs: { idList, name, desc, pos: 'top', due, idMembers, idLabels, key: config.integrations.trello.key, token }
    };

    return request(options).then(r => {
        const options = {
            method: 'POST',
            url: 'https://api.trello.com/1/cards/' + r.id + '/attachments',
            qs: { name: 'GitLab Issue', url: 'https://gitlab.com/umnico/dev/scrm/issues/' + iid, key: config.integrations.trello.key, token }
        };

        return request(options);
    });
}

function updateIssue({ token }, id, { idList, name, desc, due, idMembers, idLabels }) {
    var options = {
        method: 'PUT',
        url: 'https://api.trello.com/1/cards/' + id,
        qs: {
            idList,
            name,
            desc,
            due,
            idMembers: (!idMembers || !idMembers.length) ? '' : idMembers,
            idLabels: (!idLabels || !idLabels.length) ? '' : idLabels,
            key: config.integrations.trello.key,
            token
        }
    };

    return request(options);
}

module.exports = {
    check,
    getLabels,
    createLabel,
    getLists,
    createList,
    getIssues,
    createIssue,
    updateIssue,
}
