const chai = require('chai')
const fs = require('fs-extra')
const config = require('../utils/config')
const store = require('./mirror')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
const expect = chai.expect

const DIR = config.storage.path
const OBJ = { a: 12 }

describe('mirror store', () => {

    before(async () => {
        process.on('uncaughtException', () => { })
        process.on('unhandledRejection', () => { })
        await fs.remove(DIR)
    })

    it('should init dir', async () => {
        await store.init(DIR)

        expect(await fs.exists(DIR)).is.true
    })

    it('should create file', async () => {
        await store.create(OBJ)

        expect(await fs.readdir(DIR)).length(1)
    })

    it('should create next file', async () => {
        await store.create(OBJ)

        expect(await fs.readdir(DIR)).length(2)
    })

    it('should get obj', async () => {
        const res = await store.get(1)

        expect(res).deep.eq(OBJ)
    })

    it('should throw if id param is missing', async () => {
        expect(store.update()).eventually.to.throw('id param is missing');
        expect(store.update(OBJ)).eventually.to.throw('id param is missing');
    })

    it('should update obj', async () => {
        const obj = Object.assign({}, OBJ, { id: 1, b: 33 })
        await store.update(obj)
        const res = await store.get(1)

        expect(res).deep.eq(obj)
    })

    it('should remove file', async () => {
        await store.remove(1)

        expect(await fs.readdir(DIR)).length(1)
    })

})
