const config = require('../utils/config')
const fs = require('fs-extra')
const { join } = require('path')

const DIR = config.storage.path
const file = (id) => `mirror_${id}.json`

function init() {
    return fs.mkdirp(DIR)
}

function get(id) {
    return fs.readJson(join(DIR, file(id)))
}

async function create(obj) {
    const files = await fs.readdir(DIR)
    const mirrors = files.filter(f => f.includes('mirror_')).sort()
    const id = mirrors.length ? parseInt(mirrors[0].split('_')[1].replace('.json', '')) + 1 : 1
    await fs.writeJson(join(DIR, file(id)), obj)
    
    return Object.assign({}, obj, { id });
}

async function update(obj) {
    if (!obj || !obj.id) throw new Error('id param is missing')

    const db = await fs.readJson(join(DIR, file(obj.id)))
    return fs.writeJson(join(DIR, file(obj.id)), Object.assign({}, db, obj))
}

function remove(id) {
    return fs.unlink(join(DIR, file(id)))
}

module.exports = {
    init,
    get,
    create,
    update,
    remove,
}
