const Router = require('koa-router')
const router = new Router()

const triggers = require('./api/triggers');

router.prefix('/api')
router.use(triggers.routes())

module.exports = router
