const Boards = require('../board/index')
const _ = require('lodash')
const log = require('../utils/log')(module)

async function sync(board1, board2) {
    const l1 = (await Boards.getLabels(board1)).map(l => ({ name: l.name }));
    const l2 = (await Boards.getLabels(board2)).map(l => ({ name: l.name }));

    const diff = _.differenceWith(l1, l2, _.isEqual);

    for (let l of diff) {
        log.info('creating label ' + l.name)
        await Boards.createLabel(board2, l);
    }
}

module.exports = sync
