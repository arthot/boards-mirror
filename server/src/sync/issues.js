const Boards = require('../board/index')
const _ = require('lodash')
const log = require('../utils/log')(module)
const date = require('date-fns')

async function sync(mirror, iid = null) {
    const { board1, board2, mapping } = mirror;
    const lists = (await Boards.getLists(board2)).map(l => l.name);
    const labels = (await Boards.getLabels(board2));
    const l1Raw = await Boards.getIssues(board1);
    const l2Raw = await Boards.getIssues(board2);

    const l1Arr = l1Raw.map(i => convert(board1, i, lists)).filter(i => /^\/trello/m.test(i.desc));
    const l2Arr = l2Raw.map(i => convert(board2, i, null, mapping)).filter(i => i.iid);

    for (let l1 of iid ? l1Arr.filter(l => l.iid == iid) : l1Arr) {
        const l2 = l2Arr.find(i => i.iid === l1.iid);
        if (!l2 || !_.isEqual(l1, l2)) {
            const issue = {
                idList: mapping.labelToLists[l1.list],
                name: '#' + l1.iid + ' | ' + l1.name + (l1.spent ? ` {${l1.spent}} ` : '') + (l1.weight ? ` |${l1.weight}|` : ''),
                desc: l1.desc,
                due: l1.due,
                idMembers: l1.user ? [mapping.users[l1.user]] : [],
                idLabels: _.intersection(l1.labels, labels.map(l => l.name)).map(ll => labels.find(i => i.name === ll).id),
                iid: l1.iid,
            }
            const raw = l2Raw.find(i => i.name.startsWith('#' + l1.iid));
            if (raw && l2) {
                log.info('updating issue #' + l1.iid + ' at ' + board2.type, difference(l1, l2));
                await Boards.updateIssue(board2, raw.id, issue);
            } else {
                log.info('creating issue #' + l1.iid + ' at ' + board2.type);
                await Boards.createIssue(board2, issue);
            }
        }
    }
}

function convert(board, issue, lists, mapping) {
    switch (board.type) {
        case Boards.TYPES.trello: {
            const iid = TrelloIID.test(issue.name) ? parseInt(TrelloIID.exec(issue.name)[1]) : null;
            const [, spent] = TrelloSpent.test(issue.name) ? TrelloSpent.exec(issue.name) : [null, null];
            const [, weight] = TrelloWeight.test(issue.name) ? TrelloWeight.exec(issue.name) : [null, null];
            const name = issue.name.replace(TrelloIID, '').replace(TrelloSpent, '').replace(TrelloWeight, '').trim();
            const labels = issue.labels.map(l => l.name).sort();
            const list = _.findKey(mapping.labelToLists, o => o == issue.idList);
            const user = issue.idMembers.length ? _.findKey(mapping.users, o => issue.idMembers.includes(o)) : null;

            return {
                iid,
                desc: issue.desc,
                name,
                labels,
                list,
                user: user ? parseInt(user) : null,
                due: issue.due ? date.format(issue.due, 'YYYY-MM-DD') : null,
                spent: spent || null,
                weight: weight ? parseInt(weight) : null,
            }
        }
        case Boards.TYPES.gitlab: {
            return {
                iid: issue.iid,
                desc: issue.description ? issue.description.replace(/\/uploads\//gi, board.url + '/uploads/') : issue.description,
                name: issue.title,
                labels: _.difference(issue.labels, lists).sort(),
                list: (issue.state === 'closed' ? 'Closed' : _.last(_.intersection(lists, issue.labels))) || 'Backlog',
                user: issue.assignee ? issue.assignee.id : null,
                due: issue.due_date ? date.format(issue.due_date, 'YYYY-MM-DD') : null,
                spent: issue.time_stats.human_total_time_spent,
                weight: issue.weight,
            }
        }
    }
}

const TrelloIID = /^#(\d+) \|/
const TrelloSpent = /{(.+)\}/
const TrelloWeight = /\|(\d+)\|/

function difference(object, base) {
    return _.transform(object, function (result, value, key) {
        if (!_.isEqual(value, base[key])) {
            result[key] = (_.isObject(value) && _.isObject(base[key])) ? difference(value, base[key]) : value;

            if (typeof result[key] === 'string' && result[key].length > 30)
                result[key] = result[key].substr(0, 30) + '...';
        }
    })
}

module.exports = sync
