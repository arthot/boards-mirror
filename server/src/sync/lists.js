const Boards = require('../board/index')
const _ = require('lodash')
const log = require('../utils/log')(module)

async function sync(board1, board2) {
    const l1 = (await Boards.getLists(board1)).map(l => ({ name: l.label.name, pos: l.position + 10 }));
    const l2 = (await Boards.getLists(board2)).map(l => ({ name: l.name, pos: l.pos }));

    const diff = _.differenceWith(l1, l2, _.isEqual);

    for (let l of diff) {
        log.info('creating list ' + l.name)
        await Boards.createList(board2, l);
    }
}

module.exports = sync
