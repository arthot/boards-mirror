const { join } = require('path')
const { existsSync } = require('fs')
const nconf = require('nconf')

const CONFIG_DIR = '../../config'

const env = process.env.NODE_ENV || 'development'
const defaultConfig = join(__dirname, CONFIG_DIR, 'config.default.json')
const envConfig = join(__dirname, CONFIG_DIR, `config.${env.trim()}.json`)

if (existsSync(envConfig))
    nconf.file('override', envConfig)

nconf.env('__')
    .argv()
    .file('default', defaultConfig)

module.exports = {
    node: {
        port: nconf.get('node:port'),
    },
    storage: {
        path: nconf.get('storage:path'),
    },
    log: nconf.get('log'),
    integrations: {
        trello: {
            key: nconf.get('integrations:trello:key'),
        }
    }
}
