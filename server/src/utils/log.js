const winston = require('winston');
const config = require('./config')

const getLogger = (module) => new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            colorize: true,
            label: module.filename.split(/[/\\]/).slice(-1).join('/').replace('.js', ''),
            level: config.log
        }),
    ]
})

module.exports = getLogger;
