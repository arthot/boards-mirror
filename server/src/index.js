const Koa = require('koa')
const BodyParser = require('koa-bodyparser')
const router = require('./routes')
const config = require('./utils/config')
const log = require('./utils/log')(module)

const app = new Koa()

app.use(BodyParser({
    enableTypes: ['json'],
    strict: true,
    onerror: function (err, ctx) {
        ctx.throw('body parse error', 422)
    }
}))

app.use(router.routes())
app.use(router.allowedMethods())

process.on('uncaughtException', (err) => log.fatal(err))
process.on('unhandledRejection', (err) => log.error(err))

const server = app.listen(config.node.port, 'localhost', () => log.info(`API server started on ${config.node.port}`))

module.exports = server
