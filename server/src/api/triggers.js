const Router = require('koa-router')
const router = new Router()
const log = require('../utils/log')(module)
const sync = require('../sync/issues')
const Mirror = require('../store/mirror')

router
    .prefix('/triggers')
    .post('/:id/issue', async (ctx) => {
        const iid = ctx.request.body.object_attributes.iid

        log.debug(`webhook for ${iid}`)
        const mirror = await Mirror.get(ctx.params.id);
        if (!mirror) {
            log.error(`Mirror configuration "${ctx.params.id}" is not found`)
            return
        }

        await sync(mirror, iid)

        ctx.status = 200
    })

module.exports = router
