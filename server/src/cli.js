const argv = require('minimist')(process.argv.slice(2))
const log = require('./utils/log')(module)
const Mirror = require('./store/mirror')

if (!argv.m) throw new Error('Mirror id param is required')

async function run() {
    const mirror = await Mirror.get(argv.m);

    if (argv.check) {
        const Boards = require('./board/index')
        const check = async (board) => {
            log.debug('checking board ' + board.id + ' at ' + board.type)
            if (! await Boards.check(board))
                log.error('cannot connect to board ' + board.id);
            else
                log.debug('board at ' + board.type + ' is ok')
        }

        await Promise.all([
            check(mirror.board1),
            check(mirror.board2)
        ])
    }
    if (argv.labels) {
        log.debug('syncing labels')
        const sync = require('./sync/labels')
        await sync(mirror.board1, mirror.board2)
        log.debug('ok')
    }
    if (argv.lists) {
        log.debug('syncing lists')
        const sync = require('./sync/lists')
        await sync(mirror.board1, mirror.board2)
        log.debug('ok')
    }
    if (argv.issues) {
        log.debug('syncing issues')
        const sync = require('./sync/issues')
        await sync(mirror)
        log.debug('ok')
    }
}

run();
